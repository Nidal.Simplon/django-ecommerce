from django.conf import settings
from django.contrib import admin
from django.urls import path, include
from .views import ItemDetailView, CheckoutView, products, add_to_cart, remove_from_cart, OrderSummaryView, remove_single_item_from_cart
app_name = 'dj'

urlpatterns = [
    path('admin/', admin.site.urls),
    path('accounts/', include('allauth.urls')),
    path('', include('core.urls', namespace='core')),
    path('checkout/', CheckoutView.as_view(), name='checkout'),
    path('order-summary/', OrderSummaryView.as_view(), name='order_summary'),
    path('product/<slug>/', ItemDetailView.as_view(), name='product'),
    path('products/', products, name='products'),
    path('add-to-cart/<slug>/', add_to_cart, name='add_to_cart'),
    path('remove_from_cart/<slug>', remove_from_cart, name='remove_from_cart'),
    path('remove_single_item_from_cart/<slug>',
         remove_single_item_from_cart, name='remove_single_item_from_cart'),




]

if settings.DEBUG:
    import debug_toolbar
    urlpatterns += [path('__debug__', include(debug_toolbar.urls))]
