from django.shortcuts import render
from django.views.generic import ListView
from .models import Item


class HomeView(ListView):
    model = Item
    paginate_by = 2
    template_name = "home-page.html"


# def homepage(request):
#     context = {
#         'items': Item.objects.all()
#     }

#     return render(request, "home-page.html")
